package latinCatholicPrayers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_InitializeSpeaker(t *testing.T) {
	errorStatus := InitializeSpeaker()
	assert.Nil(t, errorStatus, "This should not return an error")
}

func Test_RetrievePathToAudioFile(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"

	prayerName := "Pater Noster"
	fileName := "PaterNoster.ogg"
	returnedPath := RetrievePathToAudioFile(prayerName)
	assert.Contains(t, returnedPath, fileName, "Should contain the file name")
}
func Test_PlayErrorSound(t *testing.T) {
	ConfigurationFile.SoundDirectory = "Sounds"
	returnedError := PlayErrorSound()
	assert.Nil(t, returnedError, "Should be nil")
	ConfigurationFile.SoundDirectory = "NotExist"
	returnedError = PlayErrorSound()
	assert.Nil(t, returnedError, "It just will not play should still be no error")
}

func Test_PlayPrayerFile(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	prayerName := "Signum Crucis"
	returnedError := PlayPrayerFile(prayerName)
	assert.Nil(t, returnedError, "This should be able to do the prayer")
}

func Test_PlayWordFromPrayer(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	wordName := "et"
	returnedError := PlayWordFromPrayer(wordName)
	assert.Nil(t, returnedError, "This should be able to do the prayer")
}

func Test_playAudioFile(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	prayerName := "DevilChant"
	returnedError := queueSoundSample(prayerName)
	assert.NotNil(t, returnedError, "This should return an error and does not")
}

func Test_RetrievePathToSpokenLatinWord(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	wordName := "ave"
	soundFile := "ave.ogg"
	pathToFile := RetrievePathToSpokenLatinWord(wordName)
	assert.Contains(t, pathToFile, soundFile, "Should contain the file for ave maria")
}

func Test_fileExists(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.SoundDirectory = "Sounds"

	prayerName := "Pater Noster"
	pathToFile := RetrievePathToAudioFile(prayerName)
	assert.True(t, fileExists(pathToFile), "This file should exist")
	assert.False(t, fileExists("Iamnotreal.txt"), "No real file is here")
}
