package latinCatholicPrayers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// MyHelp ... Help files instance for singleton object
var MyHelp HelpFiles

var FileServerIsEnabled bool = false

const HelpFileJson string = "HelpFiles.json"

type HelpFiles map[string]interface{}

func (help *HelpFiles) startServer() {
	fs := http.FileServer(http.Dir(ConfigurationFile.HelpFiles))
	http.Handle("/", fs)
	err := http.ListenAndServe(":5999", nil)
	if err == nil {
		FileServerIsEnabled = true
	}

}

func (help *HelpFiles) populateMapOfNamesToVideos() (err error) {
	if !FileServerIsEnabled {
		go help.startServer()
	}
	mapOfFilesToVideosJsonFilePath := ConfigurationFile.HelpFiles + PS + HelpFileJson
	jsonFile, err := os.Open(mapOfFilesToVideosJsonFilePath)
	if err == nil {
		byteArray, err := ioutil.ReadAll(jsonFile)
		if err == nil {
			json.Unmarshal(byteArray, help)
		}
	}
	defer jsonFile.Close()
	return
}

func (help *HelpFiles) GetListOfVideos() (listOfVideos []string) {
	var localListOfVideos StrSlice
	if len(*help) == 0 {
		err := help.populateMapOfNamesToVideos()
		if err != nil {
			return
		}
	}

	for key, element := range *help {
		if !localListOfVideos.Has(key) {
			localListOfVideos = append(localListOfVideos, key)
		}
		_ = element // do not need it
	}
	listOfVideos = localListOfVideos
	return
}

func (help *HelpFiles) GetFileNameFromVideoName(videoName string) (fileName string) {
	if len(*help) == 0 {
		err := help.populateMapOfNamesToVideos()
		if err != nil {
			return
		}
	}

	helpReference := *help
	fileName = fmt.Sprintf("%v", helpReference[videoName])
	return
}
