package latinCatholicPrayers

import (
	"os"
	"strings"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/vorbis"
)

// Queue ... A queue of streamers for the program
type Queue struct {
	streamers []beep.Streamer
}

// Add ... This adds a streamer to the Queue
func (q *Queue) Add(streamers ...beep.Streamer) {
	q.streamers = append(q.streamers, streamers...)
}

// SoundQueue ... This is a queue to hold all of the streamers we need to use
var SoundQueue Queue

// SampleRate ... This adds the sample rate variable
var SampleRate beep.SampleRate

// Stream ... This is the Stream function for the Queue type
func (q *Queue) Stream(samples [][2]float64) (n int, ok bool) {
	filled := 0
	for filled < len(samples) {
		if len(q.streamers) == 0 {
			for i := range samples[filled:] {
				samples[i][0] = 0
				samples[i][1] = 0
			}
			break
		}

		n, ok := q.streamers[0].Stream(samples[filled:])
		if !ok {
			q.streamers = q.streamers[1:]
		}
		filled += n
	}
	return len(samples), true
}

// Err ... Trivial error function for queue
func (q *Queue) Err() error {
	return nil
}

// PlayPrayerFile ... plays the prayer based on the provided name
func PlayPrayerFile(name string) (err error) {
	fileName := RetrievePathToAudioFile(name)
	if fileExists(fileName) {
		err = queueSoundSample(fileName)
	}
	return
}

// PlayErrorSound ... Plays the sound indicating a bad choice
func PlayErrorSound() (err error) {
	fileName := ConfigurationFile.SoundDirectory + PS + "SpecialEffects" + PS + "Buzzer.ogg"
	if fileExists(fileName) {
		err = queueSoundSample(fileName)
	}
	return
}

// RetrievePathToSpokenLatinWord ... Retrieves the path to the word to be played
func RetrievePathToSpokenLatinWord(word string) (audioFile string) {
	audioFile = ConfigurationFile.SoundDirectory + PS + "Words" + PS + convertToPlainText(word) + ".ogg"
	return
}

// PlayWordFromPrayer ... Plays a word from a prayer
func PlayWordFromPrayer(word string) (err error) {

	fileName := RetrievePathToSpokenLatinWord(word)
	if fileExists(fileName) {
		err = queueSoundSample(fileName)
	}
	return
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func convertToPlainText(wordToConvert string) (convertedWord string) {

	convertedWord = wordToConvert
	specialToNormalCharacters := map[string]string{
		"æ": "ae",
		"á": "aa",
		"ë": "eh",
		"é": "ee",
		"í": "ii",
		"ó": "oo",
		"ú": "uu",
		"œ": "oe",
		"Í": "ii",
	}
	for specialCharacter, normalCharacter := range specialToNormalCharacters {
		convertedWord = strings.ReplaceAll(convertedWord, specialCharacter, normalCharacter)
	}

	return
}

// InitializeSpeaker ... Initializes the speaker for playback
func InitializeSpeaker() (err error) {
	SampleRate = beep.SampleRate(44100)
	err = speaker.Init(SampleRate, SampleRate.N(time.Second/10))
	if err == nil {
		speaker.Play(&SoundQueue)
	}
	return
}

func queueSoundSample(name string) (err error) {
	f, err := os.Open(name)
	if err != nil {
		return err
	}
	streamer, format, err := vorbis.Decode(f)
	if err != nil {
		return err
	}

	resampled := beep.Resample(4, format.SampleRate, SampleRate, streamer)
	speaker.Lock()
	SoundQueue.Add(resampled)
	speaker.Unlock()
	return
}

// RetrievePathToAudioFile ... Retrieves a prayer based on
func RetrievePathToAudioFile(name string) (audioFile string) {
	listOfPrayerPaths, err := ListOfPrayerPaths()
	if err == nil {
		for _, prayerPath := range listOfPrayerPaths {
			nameFound := GetLatinNameFromPrayerPathAndExpectedName(prayerPath, name)
			if strings.ToLower(nameFound) == strings.ToLower(name) {
				prayer, err := GetPrayerFromPrayerPath(prayerPath)
				if err == nil {
					audioFile = ConfigurationFile.SoundDirectory + PS + "Prayers" + PS + prayer.AudioFile
					break
				}
			}
		}
	}
	return
}
