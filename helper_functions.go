package latinCatholicPrayers

import (
	"math"
	"strconv"
	"strings"
	"unicode/utf8"
)

// StrSlice .... This is a type to hold an array of strings
type StrSlice []string

// Has ... This is a property for StrSlice that will either return true or false based on whether the string has
// a string in it
func (list StrSlice) Has(a string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// VersionType ... This is a datatype for a version
type VersionType string

func (version VersionType) trimFirstRune() (outputVersion VersionType) {
	versionString := strings.ReplaceAll(string(version), " ", "")
	versionString = strings.ReplaceAll(versionString, "\n", "")
	_, i := utf8.DecodeRuneInString(versionString)
	if (rune(versionString[0]) == 'v') || (rune(versionString[0]) == 'V') {
		versionString = versionString[i:]
	}
	outputVersion = VersionType(versionString)
	return
}

// NumericalValue ... This reduces a version into a straight integer value
func (version VersionType) NumericalValue() (accumulator int) {

	version = VersionType(version.trimFirstRune())
	splitVersion := strings.Split(string(version), ".")

	for index, part := range splitVersion {
		numberForPart, err := strconv.Atoi(part)
		if err != nil {
			numberForPart = 0
		}
		lengthOfVersion := len(splitVersion)
		multiplier := int(math.Pow(10000, float64(lengthOfVersion-index)))
		accumulator = accumulator + numberForPart*multiplier
	}

	return

}

// GreaterThan ... This makes checks one version against another to see if it is greater than
// that version
func (version VersionType) GreaterThan(anotherVersion VersionType) (result bool) {
	numericalValueOneVersion := version.NumericalValue()
	numericalValueAnotherVersion := anotherVersion.NumericalValue()
	result = numericalValueOneVersion > numericalValueAnotherVersion
	return
}
