package latinCatholicPrayers

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetPrayersFromUser(t *testing.T) {
	ConfigurationFile.UsersDirectory = "./UserResultsTest"
	listOfPrayers, exists := GetPrayersFromUser("superman")

	assert.True(t, exists)
	assert.Contains(t, listOfPrayers, "Pater Noster")
}

func Test_directoryExists(t *testing.T) {
	userHomeDir, err := os.UserHomeDir()
	testResult := directoryExists(userHomeDir)
	assert.True(t, testResult, "User has a home dir")
	assert.Nil(t, err, "This should be nil")
}

func Test_SetGroupToDisplay(t *testing.T) {
	SetGroupToDisplay("Grapes")
	assert.Equal(t, "Grapes", SelectedGroupForLogDisplay)
}

func Test_GetPrayerPathFromName(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	nameToCheck := "Pater Noster"
	expectedPrayerPath := "Prayers/PaterNoster.json"
	actualPrayerPath := GetPrayerPathFromName(nameToCheck)
	assert.Equal(t, expectedPrayerPath, actualPrayerPath)
}

func Test_AddNewUser(t *testing.T) {

	ConfigurationFile.UsersDirectory = "UserTestingUsers"
	newUser := "Dracula Bramstokers"
	newFile := ConfigurationFile.UsersDirectory + PS + "Dracula_Bramstokers.json"
	err := AddNewUser(newUser)
	assert.Nil(t, err, "This should be nil")
	assert.FileExists(t, newFile, "File should have been created")
	os.Remove(newFile)

	ConfigurationFile.UsersDirectory = "bluhbluh"

	newUser = "Dracula Bramstokers"
	newFile = ConfigurationFile.UsersDirectory + PS + "Dracula_Bramstokers.json"
	err = AddNewUser(newUser)
	assert.Nil(t, err, "This should be nil")
	assert.FileExists(t, newFile, "File should have been created")
	os.RemoveAll("bluhbluh")
}

func Test_ListOfUserResultPaths(t *testing.T) {
	ConfigurationFile.UsersDirectory = "UnitTestingUsers"
	newUserOne := "Dracula Vampire"
	newUserTwo := "Werewolf Wolfie"
	newUserThree := "Flying Monkey"
	newFileOne := ConfigurationFile.UsersDirectory + PS + newUserOne
	newFileTwo := ConfigurationFile.UsersDirectory + PS + newUserTwo
	newFileThree := ConfigurationFile.UsersDirectory + PS + newUserThree

	errOne := AddNewUser(newUserOne)
	errTwo := AddNewUser(newUserTwo)
	errThree := AddNewUser(newUserThree)

	assert.Nil(t, errOne)
	assert.Nil(t, errTwo)
	assert.Nil(t, errThree)

	assert.Contains(t, newFileOne, newUserOne)
	assert.Contains(t, newFileTwo, newUserTwo)
	assert.Contains(t, newFileThree, newUserThree)

	listOfUserResultPaths := ListOfUserResultPaths()
	assert.Contains(t, listOfUserResultPaths[0], "Dracula")
	assert.Contains(t, listOfUserResultPaths[1], "Monkey")
	assert.Contains(t, listOfUserResultPaths[2], "Werewolf")
	assert.Len(t, listOfUserResultPaths, 3)

	listOfUsers := ListOfUsersWithAll()
	assert.Contains(t, listOfUsers, "All")
	assert.Contains(t, listOfUsers, newUserThree)

	os.RemoveAll(ConfigurationFile.UsersDirectory)
}

func Test_ListOfUsersWithAll(t *testing.T) {
	ConfigurationFile.UsersDirectory = "UnitTestingUsers"
	newUserOne := "Dracula Vampire"
	newUserTwo := "Werewolf Wolfie"
	newUserThree := "Flying Monkey"
	newFileOne := ConfigurationFile.UsersDirectory + PS + newUserOne
	newFileTwo := ConfigurationFile.UsersDirectory + PS + newUserTwo
	newFileThree := ConfigurationFile.UsersDirectory + PS + newUserThree

	errOne := AddNewUser(newUserOne)
	errTwo := AddNewUser(newUserTwo)
	errThree := AddNewUser(newUserThree)

	assert.Nil(t, errOne)
	assert.Nil(t, errTwo)
	assert.Nil(t, errThree)

	assert.Contains(t, newFileOne, newUserOne)
	assert.Contains(t, newFileTwo, newUserTwo)
	assert.Contains(t, newFileThree, newUserThree)

	listOfUsers := ListOfUsers()

	assert.Contains(t, listOfUsers, newUserOne)
	assert.Contains(t, listOfUsers, newUserTwo)
	assert.Contains(t, listOfUsers, newUserThree)

	assert.NotContains(t, listOfUsers, "Nobody in particular")
	os.RemoveAll(ConfigurationFile.UsersDirectory)
}

func Test_ListOfUsers(t *testing.T) {
	ConfigurationFile.UsersDirectory = "UnitTestingUsers"
	newUserOne := "Dracula Vampire"
	newUserTwo := "Werewolf Wolfie"
	newUserThree := "Flying Monkey"
	newFileOne := ConfigurationFile.UsersDirectory + PS + newUserOne
	newFileTwo := ConfigurationFile.UsersDirectory + PS + newUserTwo
	newFileThree := ConfigurationFile.UsersDirectory + PS + newUserThree

	errOne := AddNewUser(newUserOne)
	errTwo := AddNewUser(newUserTwo)
	errThree := AddNewUser(newUserThree)

	assert.Nil(t, errOne)
	assert.Nil(t, errTwo)
	assert.Nil(t, errThree)

	assert.Contains(t, newFileOne, newUserOne)
	assert.Contains(t, newFileTwo, newUserTwo)
	assert.Contains(t, newFileThree, newUserThree)

	listOfUsers := ListOfUsers()

	assert.Contains(t, listOfUsers, newUserOne)
	assert.Contains(t, listOfUsers, newUserTwo)
	assert.Contains(t, listOfUsers, newUserThree)

	assert.NotContains(t, listOfUsers, "Nobody in particular")
	os.RemoveAll(ConfigurationFile.UsersDirectory)
}

func Test_GetListOfUniquePrayersThatAreInGroup(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	prayers := GetListOfUniquePrayersThatAreInGroup("superman", "Rosary")
	assert.Contains(t, prayers, "Pater Noster")

}
func Test_GetCurrentScores(t *testing.T) {
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	currentResults := GetCurrentScores("All", "superman", "Pater Noster", "All")
	assert.Contains(t, currentResults[0], "Pater Noster")
	assert.Contains(t, currentResults[0], "0%")
	currentResults = GetCurrentScores("All", "superman", "Pater Noster", "Average")
	assert.Contains(t, currentResults[0], "43%")
	currentResults = GetCurrentScores("All", "superman", "Pater Noster", "Last")
	assert.Contains(t, currentResults[0], "100%")
}

func Test_GetListOfGroupsForUniquePrayers(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	groups := GetListOfGroupsForUniquePrayers("superman")
	assert.Contains(t, groups, "Rosary")
	assert.NotContains(t, groups, "Devils")
}

func Test_AddResultsToRecord(t *testing.T) {
	ConfigurationFile.UsersDirectory = "UnitTestingUsers"
	newUserOne := "Dracula Vampire"
	newUserTwo := "Werewolf Wolfie"
	newUserThree := "Flying Monkey"

	AddNewUser(newUserOne)
	AddNewUser(newUserTwo)
	AddNewUser(newUserThree)

	draculaPrayerResults := PrayerScoreType{"Dracula Prayer", "100%", "10/22/2020"}
	werewolfPrayerResults := PrayerScoreType{"Werewolf Prayer", "100%", "10/22/2020"}
	flyingMonkeyPrayerResults := PrayerScoreType{"Flying Prayer", "100%", "10/22/2020"}
	AddResultsToRecord(newUserOne, draculaPrayerResults)
	AddResultsToRecord(newUserTwo, werewolfPrayerResults)
	AddResultsToRecord(newUserThree, flyingMonkeyPrayerResults)
	AddResultsToRecord("No such user", draculaPrayerResults)

	resultsFromDracula := GetResultsFromUser(newUserOne)
	assert.Contains(t, resultsFromDracula.Scores[0].PrayerName, "Dracula")
	assert.NotContains(t, resultsFromDracula.Scores[0].PrayerName, "Werewolf")
	os.RemoveAll(ConfigurationFile.UsersDirectory)

}
