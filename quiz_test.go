package latinCatholicPrayers

import (
	"reflect"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetListOfEnglishWords(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		prayer string
	}

	goodPrayerLatinName := args{"LatinName"}
	goodPrayerLatinName2 := args{"LatinName2"}
	goodPrayerEnglishName := args{"EnglishName"}
	goodPrayerEnglishName2 := args{"EnglishName2"}
	expectedWordListTestPrayer := []string{"and", "of", "the", "son"}
	expectedWordListTestPrayer2 := []string{"in", "the", "name", "of", "the", "father"}

	tests := []struct {
		name            string
		args            args
		wantListOfWords []string
	}{
		{"TestPrayer", goodPrayerLatinName, expectedWordListTestPrayer},
		{"TestPrayer2", goodPrayerLatinName2, expectedWordListTestPrayer2},
		{"TestPrayer", goodPrayerEnglishName, expectedWordListTestPrayer},
		{"TestPrayer2", goodPrayerEnglishName2, expectedWordListTestPrayer2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotListOfWords := GetListOfEnglishWords(tt.args.prayer); !reflect.DeepEqual(gotListOfWords, tt.wantListOfWords) {
				t.Errorf("GetListOfEnglishWords() = %v, want %v", gotListOfWords, tt.wantListOfWords)
			}
		})
	}
}

func Test_GetListOfLatinWords(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		prayer string
	}
	goodPrayerLatinName := args{"LatinName"}
	goodPrayerLatinName2 := args{"LatinName2"}
	goodPrayerEnglishName := args{"EnglishName"}
	goodPrayerEnglishName2 := args{"EnglishName2"}
	expectedWordListTestPrayer := []string{"et", "filii"}
	expectedWordListTestPrayer2 := []string{"in", "nomine", "patris"}

	tests := []struct {
		name            string
		args            args
		wantListOfWords []string
	}{
		{"LatinNameTestCase1", goodPrayerLatinName, expectedWordListTestPrayer},
		{"LatinNameTestCase2", goodPrayerLatinName2, expectedWordListTestPrayer2},
		{"EnglishNameTestCase1", goodPrayerEnglishName, expectedWordListTestPrayer},
		{"EnglishNameTestCase2", goodPrayerEnglishName2, expectedWordListTestPrayer2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotListOfWords := GetListOfLatinWords(tt.args.prayer); !reflect.DeepEqual(gotListOfWords, tt.wantListOfWords) {
				t.Errorf("GetListOfLatinWords() = %v, want %v", gotListOfWords, tt.wantListOfWords)
			}
		})
	}
}

func Test_setToLowerCase(t *testing.T) {
	type args struct {
		listOfWords []string
	}
	listWithCases := []string{"Apples", "Oranges", "Grapes"}
	goodList := []string{"apples", "oranges", "grapes"}
	withCase := args{listWithCases}

	tests := []struct {
		name                     string
		args                     args
		wantListOfWordsLowerCase []string
	}{
		{"SetToLowerCase", withCase, goodList},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotListOfWordsLowerCase := setToLowerCase(tt.args.listOfWords); !reflect.DeepEqual(gotListOfWordsLowerCase, tt.wantListOfWordsLowerCase) {
				t.Errorf("setToLowerCase() = %v, want %v", gotListOfWordsLowerCase, tt.wantListOfWordsLowerCase)
			}
		})
	}
}

func Test_findItem(t *testing.T) {
	type args struct {
		a []string
		x string
	}

	listToSearch := []string{"Apples", "Oranges", "Grapes"}
	stringToFindGood := "Apples"
	stringToFindBad := "Pears"
	goodSearch := args{listToSearch, stringToFindGood}
	badSearch := args{listToSearch, stringToFindBad}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Test good search", goodSearch, 0},
		{"Test bad search", badSearch, -1},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findItem(tt.args.a, tt.args.x); got != tt.want {
				t.Errorf("findItem() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_shuffleList(t *testing.T) {
	listToShuffle := []string{"Apples", "Grapes", "Pears", "Peas"}
	shuffledList := shuffleList(listToShuffle)
	if len(listToShuffle) != len(shuffledList) {

		t.Fail()
	}

}

func Test_getListOfOptions(t *testing.T) {

	listToGetOptionFrom := []string{"Apples", "Grapes", "Pears", "Peas", "Corn", "Greens", "Pickles", "Pickles", "Grapes", "Oranges"}
	listOfOptions := getListOfOptions(listToGetOptionFrom, 0)

	assert.Equal(t, len(listOfOptions.options), numberOfOptions, "Number of options should be equal to actual number of options")
	assert.Equal(t, listOfOptions.options[listOfOptions.correctItem], listToGetOptionFrom[0], "The first item should be correct")
}

func Test_removeDuplicateStrings(t *testing.T) {
	testSet := []string{"Apples", "Grapes", "Grapes", "Pears", "Pears", "Almonds", "Almonds", "Apples", "Apples", "Apples"}
	expectedSet := []string{"Apples", "Grapes", "Pears", "Almonds"}
	resultantSet := removeDuplicateStrings(testSet)

	sort.Strings(expectedSet)
	sort.Strings(resultantSet)
	assert.Equal(t, expectedSet, resultantSet, "If I remove all of the duplicates these should be the same.")
	assert.False(t, thereAreDuplicates(resultantSet), "There should be no duplciates")

}

func Test_GetListOfLatinOptions(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	SelectedPrayer = "Ave Maria"
	listOfOptions, err := GetListOfLatinOptions(0)
	assert.Equal(t, len(listOfOptions.options), numberOfOptions, "We should have the correct number of options")
	assert.LessOrEqual(t, listOfOptions.correctItem, len(listOfOptions.options), "We should have fewer than the number of items for the correct item")
	assert.Nil(t, err, "No error should be reported")
	assert.Contains(t, listOfOptions.options[listOfOptions.correctItem], "ave", "ave should be the in the list of options for the options.")
	listOfOptions, err = GetListOfLatinOptions(1)
	assert.Contains(t, listOfOptions.options[listOfOptions.correctItem], "maría", "maria should be in the list of options for the options ")
	assert.NotContains(t, listOfOptions.options[listOfOptions.correctItem], "pater", "maria should be in the list of options for the options ")
	assert.False(t, thereAreDuplicates(listOfOptions.options))

}
func Test_GetistOfEnglishOptions(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	SelectedPrayer = "Hail Mary"
	listOfOptions, err := GetListOfEnglishOptions(0)
	assert.Equal(t, len(listOfOptions.options), numberOfOptions, "We should have the correct number of options")
	assert.LessOrEqual(t, listOfOptions.correctItem, len(listOfOptions.options), "We should have fewer than the number of items for the correct item")
	assert.Nil(t, err, "No error should be reported")
	assert.Contains(t, listOfOptions.options[listOfOptions.correctItem], "hail", "ave should be the in the list of options for the options.")
	listOfOptions, err = GetListOfEnglishOptions(1)
	assert.Contains(t, listOfOptions.options[listOfOptions.correctItem], "mary", "maria should be in the list of options for the options ")
	assert.False(t, thereAreDuplicates(listOfOptions.options))

}

func Test_CheckSelection(t *testing.T) {
	listOfOptions := []string{"Apples", "Oranges", "Grapes", "Pears"}
	correctItem := 1
	itemIsCorrect := CheckSelection(listOfOptions, correctItem, "Oranges")

	assert.True(t, itemIsCorrect, "This item should be correct")
	itemIsNotCorrect := CheckSelection(listOfOptions, correctItem, "Stranger")
	assert.False(t, itemIsNotCorrect, "This item should not be correct")
	itemIsNotCorrect = CheckSelection(listOfOptions, 30, "Stranger")
	assert.False(t, itemIsNotCorrect, "This item should not be correct")
}

func Test_GetFullLatinQuiz(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	SetNumberOfOptions(3)
	latinQuiz := GetFullLatinQuiz("Our Father")
	listOfWords := GetListOfLatinWords("Our Father")

	if len(latinQuiz) < numberOfOptions {
		t.Errorf("If number Of total elements is < number of options this is impossible.")
	}

	for index, item := range latinQuiz {
		if findItem(item.options, listOfWords[index]) == -1 {
			t.Errorf("If the correct word is not present in the list, we have a problem houston")
		}
		if len(item.options) != 3 {
			t.Errorf("The wrong number of options is present")
		}
	}
}

func Test_GetFullEnglishnQuiz(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	SetNumberOfOptions(3)
	EnglishQuiz := GetFullEnglishQuiz("Our Father")
	listOfWords := GetListOfEnglishWords("Our Father")

	if len(EnglishQuiz) < numberOfOptions {
		t.Errorf("If number Of total elements is < number of options this is impossible.")
	}

	for index, item := range EnglishQuiz {
		if findItem(item.options, listOfWords[index]) == -1 {
			t.Errorf("If the correct word is not present in the list, we have a problem houston")
		}
		if len(item.options) != 3 {
			t.Errorf("The wrong number of options is present")
		}
	}

}

func Test_InitializeLatinQuiz(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	isNotInitialized := !InitializeLatinQuiz("Our Father")
	if isNotInitialized {
		t.Errorf("This should have easily initialized")
	}
	isInitialized := InitializeLatinQuiz("UnknownPrayer")
	if isInitialized {
		t.Errorf("This prayer does not exist and should fail to initialize!")
	}
}

func Test_InitializeEnglishQuiz(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	isNotInitialized := !InitializeEnglishQuiz("Our Father")
	if isNotInitialized {
		t.Errorf("This should have easily initialized")
	}
	isInitialized := InitializeEnglishQuiz("UnknownPrayer")
	if isInitialized {
		t.Errorf("This prayer does not exist and should fail to initialize!")
	}

}

func Test_AdministerQuiz(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	isInitialized := InitializeLatinQuiz("Our Father")
	listOfWords := GetListOfLatinWords("Our Father")
	finalScore := ""
	if isInitialized == true {
		for i := 0; i < len(currentQuiz); i++ {
			AdministerQuiz(listOfWords[i])
		}
	}

	if currentScore != currentQuizItem {
		t.Errorf("current score should equal currentquizitem only once the test is finished")
	}

	finalScore = ReportCurrentScore()

	if finalScore != "100%" {
		t.Errorf("current score should equal 100%% ")
	}

	isInitialized = InitializeLatinQuiz("Our Father")
	finalScore = ""

	if isInitialized == true {
		for i := 0; i < len(currentQuiz); i++ {
			AdministerQuiz("blubluhbluhhllll")
		}
	}
	finalScore = ReportCurrentScore()

	if finalScore != "0%" {
		t.Errorf("I was expecting 0 percent not %s", finalScore)
	}

}

func Test_GetCurrentOptions(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	isInitialized := InitializeLatinQuiz("Our Father")
	listOfWords := GetListOfLatinWords("Our Father")
	if isInitialized == true {
		for i := 0; i < 3; i++ {
			AdministerQuiz(listOfWords[i])
		}
	}

	listOfOptions := GetCurrentOptions()

	if len(listOfOptions) != numberOfOptions {
		t.Errorf("This must have 5 options")
	}
}
