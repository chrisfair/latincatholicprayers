package latinCatholicPrayers

import (
	"errors"
	"fmt"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

// PrayerOptionType ... Type to contain the options array and the integer
type PrayerOptionType struct {
	options              []string
	correctItem          int
	numberOfWrongGuesses int
}

// LastResponseIsFinished ... This is used to help guard against calling AdministerQuiz again
// before its last response has been dealt with.
var LastResponseIsFinished = true

var numberOfOptions int = 5
var lastAnswerWasCorrect bool = false

// SelectedPrayer ... This allows the Selected Prayer to be none during the quiz
var SelectedPrayer string = "Hail Mary"
var currentQuiz []PrayerOptionType
var numberOfQuestions int = 0
var numberOfWrongAnswers int = 0
var currentScore int = 0
var currentQuizItem int = 0
var completedQuiz string = ""

// StripPunctuation ... This strips the punctuation fro a word
func StripPunctuation(wordToStrip string) (strippedWord string) {
	reg, err := regexp.Compile("[^a-zA-Z0-9æáéíóúœÍ]+")
	if err != nil {
		return
	}
	stringToReplace := wordToStrip
	strippedWord = reg.ReplaceAllString(stringToReplace, "")
	return
}
func stripPunctuation(listOfWords []string) (strippedListOfWords []string) {
	lengthOfList := len(listOfWords)
	if lengthOfList > 0 {
		for index := 0; index < lengthOfList; index++ {
			processedString := StripPunctuation(listOfWords[index])
			strippedListOfWords = append(strippedListOfWords, processedString)
		}
	}

	return
}

// SetToLowerCase ...
func SetToLowerCase(word string) (lowerCaseWord string) {
	lowerCaseWord = strings.ToLower(word)
	return
}

func setToLowerCase(listOfWords []string) (listOfWordsLowerCase []string) {
	lengthOfList := len(listOfWords)
	if lengthOfList > 0 {
		for index := 0; index < lengthOfList; index++ {
			listOfWordsLowerCase = append(listOfWordsLowerCase, SetToLowerCase(listOfWords[index]))
		}

	}
	return

}

// Find returns the smallest index i at which x == a[i],
// or len(a) if there is no such index.
func findItem(a []string, x string) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return -1
}

func remove(listOfItems []string, duplicate string) []string {
	for i, v := range listOfItems {
		if v == duplicate {
			return append(listOfItems[:i], listOfItems[i+1:]...)
		}
	}
	return listOfItems
}

func copySlice(initialSlice []string) (finalSlice []string) {

	for _, item := range initialSlice {
		finalSlice = append(finalSlice, item)
	}
	return finalSlice

}

func shuffleList(slice []string) (scrambledSlice []string) {
	rand.Seed(time.Now().UnixNano())
	scrambledSlice = copySlice(slice)
	rand.Shuffle(len(slice), func(i, j int) { scrambledSlice[i], scrambledSlice[j] = scrambledSlice[j], scrambledSlice[i] })
	return

}

type retrievePrayerFunction func(prayerName string) (prayerString string)

func getListOfWords(prayerFunc retrievePrayerFunction, prayerName string) (listOfWords []string) {
	prayerFound := prayerFunc(prayerName)

	if prayerFound != "" {
		listOfWords = strings.Split(prayerFound, " ")
		listOfWords = stripPunctuation(listOfWords)
		listOfWords = setToLowerCase(listOfWords)

	}
	return

}

// GetListOfEnglishWords ... This retrieves a list of English words provided the correct string
func GetListOfEnglishWords(prayer string) (listOfWords []string) {
	return getListOfWords(RetrieveEnglishPrayer, prayer)
}

// GetListOfLatinWords ... This function returns the words that make up a prayer without
// punctuation when provided the name of the prayer in either English or Latin.   It returns
// an array of these owrds in Latin
func GetListOfLatinWords(prayer string) (listOfWords []string) {
	return getListOfWords(RetrieveLatinPrayer, prayer)
}

func removeDuplicateStrings(s []string) (result []string) {
	m := make(map[string]bool)
	for _, item := range s {
		if _, ok := m[item]; ok {
		} else {
			m[item] = true
		}
	}
	for item := range m {
		result = append(result, item)
	}
	return
}

// the correct one.   The idea is to pass in a list of words, the number of options and the
// current item and receive a listOfOptions the correctItem out of that list
func getListOfOptions(wordList []string, currentItem int) (options PrayerOptionType) {

	if numberOfOptions > (len(wordList) - 3) {
		numberOfOptions = len(wordList) - 3
	}

	if numberOfOptions < 3 {
		numberOfOptions = 3
	}

	if numberOfOptions > 2 {
		trimmedList := copySlice(wordList)
		trimmedList = remove(trimmedList, wordList[currentItem])
		trimmedList = removeDuplicateStrings(trimmedList)
		shrunkenList := trimmedList[0:numberOfOptions]
		shrunkenList[numberOfOptions-1] = wordList[currentItem]
		shuffledList := shuffleList(shrunkenList)
		options.options = shuffledList
		options.correctItem = findItem(options.options, wordList[currentItem])
		options.numberOfWrongGuesses = 0
	}
	return
}

// SetNumberOfOptions ... Allows the changing of the default number of options to something other than 5
func SetNumberOfOptions(newNumberOfOptions int) {
	numberOfOptions = newNumberOfOptions
}

// SetSelectedPrayer ... Set the new prayer
func SetSelectedPrayer(newPrayer string) {
	SelectedPrayer = newPrayer
}

type getListOfWordsFunction func(prayerName string) (listOfWords []string)

func getOptions(listFunction getListOfWordsFunction, currentItem int) (options PrayerOptionType, err error) {
	wordList := listFunction(SelectedPrayer)
	if (numberOfOptions > len(wordList)) || (currentItem > len(wordList)) {
		err = errors.New("Not enough words in prayer to generate the number of options requested")
		return
	}
	options = getListOfOptions(wordList, currentItem)
	return
}

// GetListOfLatinOptions ... This retrieves a list of latin options
func GetListOfLatinOptions(currentItem int) (options PrayerOptionType, err error) {
	return getOptions(GetListOfLatinWords, currentItem)
}

// GetListOfEnglishOptions ... This retrieves a list of English options
func GetListOfEnglishOptions(currentItem int) (options PrayerOptionType, err error) {
	return getOptions(GetListOfEnglishWords, currentItem)
}

// CheckSelection ... This returns true if the selection is correct and false if it is not
func CheckSelection(listOfOptions []string, correctItem int, choice string) (isCorrectItem bool) {
	isCorrectItem = false
	if correctItem > len(listOfOptions) {
		return
	}
	if listOfOptions[correctItem] == choice {
		isCorrectItem = true
	}
	return
}

type getListOfOptionsFunction func(index int) (options PrayerOptionType, err error)

func getFullQuiz(listFunction getListOfWordsFunction, optionFunction getListOfOptionsFunction, prayerForQuiz string) (options []PrayerOptionType) {
	listOfWords := listFunction(prayerForQuiz)
	SelectedPrayer = prayerForQuiz

	for index, word := range listOfWords {
		quizItem, err := optionFunction(index)
		if err == nil {
			options = append(options, quizItem)
			_ = word // I literally do not need the word here
		}
	}
	return

}

// GetFullLatinQuiz ... This retrieves a random quiz for the given prayer
func GetFullLatinQuiz(prayerForQuiz string) (options []PrayerOptionType) {
	return getFullQuiz(GetListOfLatinWords, GetListOfLatinOptions, prayerForQuiz)
}

// GetFullEnglishQuiz ... This retrieves a random quiz for the given prayer
func GetFullEnglishQuiz(prayerForQuiz string) (options []PrayerOptionType) {
	return getFullQuiz(GetListOfEnglishWords, GetListOfEnglishOptions, prayerForQuiz)
}

type getFullQuizFunction func(prayerName string) (options []PrayerOptionType)

// initializeQuiz ... This initializes the quiz based on a passed in function and the
// chosen function
func initializeQuiz(getFullQuiz getFullQuizFunction, prayerForQuiz string) (success bool) {
	success = true
	currentQuiz = getFullQuiz(prayerForQuiz)
	numberOfQuestions = len(currentQuiz)
	completedQuiz = ""
	currentScore = 0
	currentQuizItem = 0
	if numberOfQuestions < 1 {
		success = success && false
		return
	}

	return

}

// InitializeLatinQuiz ... This initializes a latin quiz by setting the currentQuiz variable
// to a newly created random quiz
func InitializeLatinQuiz(prayerForQuiz string) (success bool) {
	return initializeQuiz(GetFullLatinQuiz, prayerForQuiz)
}

// InitializeEnglishQuiz ... This initializes a english quiz by setting the currentQuiz variable
// to a newly created random quiz
func InitializeEnglishQuiz(prayerForQuiz string) (success bool) {
	return initializeQuiz(GetFullEnglishQuiz, prayerForQuiz)
}

// InitializeQuiz ... This initializes either a latin quiz or an English quiz by inspecting the
// quiz type variable and executing the correct quiz
func InitializeQuiz(prayerForQuiz string) (success bool) {
	if CurrentLanguageSelection == English {
		success = InitializeEnglishQuiz(prayerForQuiz)

	} else {
		success = InitializeLatinQuiz(prayerForQuiz)
	}
	return
}

// GetCurrentOptions ... Returns the options on the currently tested item
func GetCurrentOptions() (listOfOptions []string) {
	if len(currentQuiz) > currentQuizItem {
		listOfOptions = currentQuiz[currentQuizItem].options
	}
	return
}

func finalizeQuiz() {
	currentQuiz = []PrayerOptionType{}
	currentQuizItem = 0
	numberOfWrongAnswers = 0
	completedQuiz = ""
	currentScore = 0
}

// GetListOfWrongAnswers ... Return a list of all of the answers that have had wrong answers
func GetListOfWrongAnswers() (badAnswers []int) {
	for _, item := range currentQuiz {
		badAnswers = append(badAnswers, item.numberOfWrongGuesses)
	}
	return
}

// AdministerQuiz ... Returns true or false depending on whether or not the string is the next answer.
func AdministerQuiz(answer string) (correct bool) {
	for !LastResponseIsFinished {
	}
	LastResponseIsFinished = false
	correct = false
	if len(currentQuiz) > currentQuizItem {
		if answer == currentQuiz[currentQuizItem].options[currentQuiz[currentQuizItem].correctItem] {
			currentScore++
			currentQuizItem++
			correct = true
		} else {
			numberOfWrongAnswers++
			currentQuiz[currentQuizItem].numberOfWrongGuesses++
		}
	} else {
		finalizeQuiz()
	}
	LastResponseIsFinished = true
	return
}

// ReportCurrentScore ... This will report the current score
func ReportCurrentScore() (score string) {

	if numberOfQuestions != 0 {
		localScore := float64(currentScore)
		localNumberOfItems := float64(numberOfQuestions + numberOfWrongAnswers)
		finalScore := localScore / localNumberOfItems
		score = fmt.Sprintf("%d%%", int64(finalScore*100))
	} else {
		score = "0%"
	}
	return
}
