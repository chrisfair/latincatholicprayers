module latinCatholicPrayers

go 1.15

require (
	github.com/BurntSushi/xgb v0.0.0-20200324125942-20f126ea2843 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/faiface/beep v1.0.2
	github.com/fatih/color v1.10.0 // indirect
	github.com/gen2brain/shm v0.0.0-20200228170931-49f9650110c5 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/go-version v1.2.1
	github.com/kbinani/screenshot v0.0.0-20191211154542-3a185f1ce18f
	github.com/leaanthony/mewn v0.10.7
	github.com/leaanthony/slicer v1.5.0 // indirect
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	github.com/pkg/browser v0.0.0-20201207095918-0426ae3fba23 // indirect
	github.com/pkg/errors v0.9.1 //indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/wailsapp/wails v1.11.0
	golang.org/x/image v0.0.0-20201208152932-35266b937fa6 // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/sys v0.0.0-20201231184435-2d18734c6014 // indirect
	gopkg.in/ini.v1 v1.62.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
