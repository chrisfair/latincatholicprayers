# Latin Catholic Prayers (LaCap)

## Motivation
I am a proud traditionalist Cathlolic who wanted to make it easy for me to memorize my prayers in
Latin and I also wanted to make it easier for my children to do the same.   Simultaneously I
became interested in web development and also wanted to learn a bit more about how to develop
properly in the Go programming language.   Since I knew most people like to have some sort of GUI
but I like command line for the most part myself I also decided that I would like to have my
program to optionally operate off of the command line rather than having to deal with a GUI.  

## GUI
Using the GUI should hopefully be pretty straightforward.   At the top of the screen are three
tabs.   The first tab is the prayers tab and under that are three drop down menus, The first is 
"Group" which has as an option "All".   I have the option for assigning a group in the json files
that define the prayers.   The default option is all but if you get a particularly large selection
of prayers it is likely that breaking them into groups could be helpful.   In any case, select 
your group, select the prayer, then select from the next drop down whether you would like the 
output in Latin or English.  There are three buttons at the bottom of the screen "Start Quiz", 
"Show Prayer", and "Read Prayer".  The prayer is ALWAYS read in Latin but the on screen display 
for the quiz and the read prayers can be in either language.

### Start Quiz
This starts the quiz.   As you progress through the quiz, if you are in Latin the Latin word will
either be pronounced if you got it right or a buzz will occur if you got it wrong.   The score
will show up in the "Start Quiz".   The buttons for selection will display just above the three
operations buttons.   Correct answers will display in a slight blue color and incorrect answers
well be displayed in progressively brighter red colors indicating how many times they were missed.

### Show Prayer
This will display the prayer.   In Latin mode the word will be pronounced if you click on a word
in the prayer.   The word will also be pronounced in Latin if the Latin and the English are
exactly the same (a surpirsingly common occurance).  

### Read Prayer
The prayer will be read in Latin.   If you have chosen English for your display the show prayer
button will still display in English but the reading will come off in Latin.   

## Adding Your Own prayers
You can add your own config.json in your .config directory under latin-catholic-prayers and that
file will indicate where the prayer files and sound files should be located.  This allows you the 
ability to modify the prayers in any way you see fit (perhaps to fix a typo).



