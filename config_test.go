package latinCatholicPrayers

import (
	"testing"
)

func Test_dirExists(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		path string
	}

	directoryExists := args{ConfigurationFile.PrayerDirectory}
	directoryDoesNotExist := args{"thisdirectorydoesnotexist"}
	tests := []struct {
		name      string
		args      args
		wantFound bool
	}{
		{"Directory exists", directoryExists, true},
		{"Directory does not exits", directoryDoesNotExist, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := dirExists(tt.args.path); gotFound != tt.wantFound {
				t.Errorf("dirExists() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}

func Test_isDir(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		directory string
	}
	directoryExists := args{ConfigurationFile.PrayerDirectory}
	directoryDoesNotExist := args{"thisdirectorydoesnotexist"}
	fileIsNotDirectory := args{"main.go"}
	tests := []struct {
		name      string
		args      args
		wantFound bool
	}{
		{"Directory exists", directoryExists, true},
		{"Directory does not exits", directoryDoesNotExist, false},
		{"File is a directory", fileIsNotDirectory, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := isDir(tt.args.directory); gotFound != tt.wantFound {
				t.Errorf("isDir() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}

func Test_isValidConfigFile(t *testing.T) {
	type args struct {
		directory string
	}

	validBaseDir := args{"." + PS}
	invalidBaseDir := args{"PrayersTest"}

	tests := []struct {
		name      string
		args      args
		wantFound bool
	}{
		{"Check local config file", validBaseDir, true},
		{"Check local config file", invalidBaseDir, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := isValidConfigFile(tt.args.directory); gotFound != tt.wantFound {
				t.Errorf("isValidConfigFile() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}

func Test_loadConfiguration(t *testing.T) {
	type args struct {
		directory string
	}
	validBaseDir := args{"." + PS}
	invalidBaseDir := args{"PrayersTest"}
	isNotADir := args{"main.go"}

	tests := []struct {
		name      string
		args      args
		wantFound bool
	}{
		{"Check local config file", validBaseDir, true},
		{"Check local config file", invalidBaseDir, false},
		{"Check local config file", isNotADir, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := loadConfiguration(tt.args.directory); gotFound != tt.wantFound {
				t.Errorf("loadConfiguration() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}

func Test_loadConfigurationFromWorkingDir(t *testing.T) {
	tests := []struct {
		name      string
		wantFound bool
	}{
		{"Test load from working dir", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := loadConfigurationFromWorkingDir(); gotFound != tt.wantFound {
				t.Errorf("loadConfigurationFromWorkingDir() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}

func Test_loadConfigurationFromUser(t *testing.T) {
	tests := []struct {
		name      string
		wantFound bool
	}{
		{"Test load from user dir", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := loadConfigurationFromUser(); gotFound != tt.wantFound {
				t.Errorf("loadConfigurationFromUser() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}

func TestLoadConfiguration(t *testing.T) {
	tests := []struct {
		name      string
		wantFound bool
	}{
		{"Test load configuration", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFound := LoadConfiguration(); gotFound != tt.wantFound {
				t.Errorf("LoadConfiguration() = %v, want %v", gotFound, tt.wantFound)
			}
		})
	}
}
