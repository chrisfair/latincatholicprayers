package latinCatholicPrayers

import (
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"

	"github.com/cavaliercoder/grab"
)

// Version ... Contains the version for the app
const Version string = "1.0.4"

// DownloadFile ... Downloads the file to the specified path from the specified url
func DownloadFile(filepath string, url string) (err error) {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return
}

// DownloadBinaryFile ... Downloads the file in binary
func DownloadBinaryFile(filepath string, url string) (response string, err error) {
	responseGet, err := grab.Get(filepath, url)
	response = responseGet.Filename
	return
}

func fetchLastVersionFile() (lastVersionFilePath string, err error) {
	fileUrl := "https://sourceforge.net/p/lacap/code/ci/master/tree/last_version?format=raw"
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return
	}
	lastVersionFilePath = homeDir + PS + ".config" + PS + "latin-catholic-prayers" + PS + "last_version"
	err = DownloadFile(lastVersionFilePath, fileUrl)
	return
}

func getNextAvailableVersion() (nextAvailableVersion string) {
	nextAvailableVersion = "0.0.0"
	versionFile, err := fetchLastVersionFile()
	if err == nil {
		byteArray, err := ioutil.ReadFile(versionFile)
		if err == nil {
			possibleVersion := string(byteArray)
			newVersion := VersionType(possibleVersion)
			currentVersion := VersionType(Version)
			if newVersion.GreaterThan(currentVersion) {
				nextAvailableVersion = string(newVersion.trimFirstRune())

			}
		}
	}
	return
}

func getNextAvailableUpdate() (updateFileName string, updateUrl string) {

	nextAvailableVersion := getNextAvailableVersion()
	sourceForge := "https://sourceforge.net/projects/lacap/files/"
	updateUrl = "linux"

	if nextAvailableVersion != "0.0.0" {
		if runtime.GOOS == "windows" {
			updateFileName = "Setup-Win10-Lacap-v" + nextAvailableVersion + ".exe"
			updateUrl = sourceForge + updateFileName + "/download"
		}
		if runtime.GOOS == "darwin" {
			updateFileName = "Lacap-macos-installer-x64-v" + nextAvailableVersion + ".pkg"
			updateUrl = sourceForge + updateFileName + "/download"
		}
		if runtime.GOOS == "linux" {
			updateFileName = "Lacap-linux-generic-installer-x64-v" + nextAvailableVersion + ".pkg"
			updateUrl = sourceForge + updateFileName + "/download"
		}
	}
	return
}
