package latinCatholicPrayers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_trimFirstRune(t *testing.T) {
	firstVersion := VersionType("V0.3.2")
	firstVersion = firstVersion.trimFirstRune()
	assert.Equal(t, firstVersion, VersionType("0.3.2"))
	firstVersion = VersionType("\nv0.3.  2     ")
	assert.NotEqual(t, firstVersion, VersionType("0.3.2"))
	firstVersion = firstVersion.trimFirstRune()
	assert.Equal(t, firstVersion, VersionType("0.3.2"))
}

func Test_NumericalValue(t *testing.T) {
	firstVersion := VersionType("0.0.0")
	firstVersion = firstVersion.trimFirstRune()
	firstVersionNumber := firstVersion.NumericalValue()
	assert.Equal(t, firstVersionNumber, 0)
	firstVersion = VersionType("1.1.1")
	firstVersionNumber = firstVersion.NumericalValue()
	assert.Equal(t, firstVersionNumber, 1000100010000)
	firstVersion = VersionType("10.10.10")
	firstVersionNumber = firstVersion.NumericalValue()
	assert.Equal(t, firstVersionNumber, 10001000100000)
	firstVersion = VersionType("10.10a.10")
	firstVersionNumber = firstVersion.NumericalValue()
	assert.Equal(t, firstVersionNumber, 10000000100000)

}

func Test_GreaterThan(t *testing.T) {
	firstVersion := VersionType("0.0.0")
	secondVersion := VersionType("100.100.100")
	thirdVersion := VersionType("0.0.9999")
	fourthVersion := VersionType("0.1.0")
	assert.False(t, firstVersion.GreaterThan(secondVersion))
	assert.True(t, secondVersion.GreaterThan(firstVersion))
	assert.True(t, fourthVersion.GreaterThan(thirdVersion))
}
