package latinCatholicPrayers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

// PrayerScoreType ... This type contains the prayer name and the score for each prayer
type PrayerScoreType struct {
	PrayerName  string
	Score       string
	DateAndTime string
}

// SelectedGroupForLogDisplay ... sets the group to display
var SelectedGroupForLogDisplay = "All"

type UserResultsType struct {
	UserName string
	Scores   []PrayerScoreType
}

func ClearResults() {
	locationOfResults := ConfigurationFile.UsersDirectory
	users := ListOfUsers()

	for _, user := range users {

		fileToRemove := locationOfResults + PS + user + ".json"
		os.Remove(fileToRemove)
	}
}

func SetGroupToDisplay(group string) {
	SelectedGroupForLogDisplay = group
}

func directoryExists(path string) (exists bool) {
	exists = false
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		exists = true
	}
	return
}

func createResultsFile(userName string, userResults *UserResultsType) (err error) {
	jsonFile, err := json.MarshalIndent(userResults, "", "\t")
	if err != nil {
		return
	}

	fileName := userName
	fileName = strings.ReplaceAll(fileName, " ", "_")

	if !directoryExists(ConfigurationFile.UsersDirectory) {
		os.MkdirAll(ConfigurationFile.UsersDirectory, os.ModePerm)
	}
	fullFilePath := ConfigurationFile.UsersDirectory + PS + fileName + ".json"
	if !fileExists(fullFilePath) {
		fileHandler, insideErr := os.Create(fullFilePath)
		if insideErr != nil {
			err = insideErr
			return
		}
		fileHandler.Write(jsonFile)
	}
	return
}

// AddNewUser ... Adds a new user to the list of users
func AddNewUser(userName string) error {
	var prayerScores []PrayerScoreType
	newUser := UserResultsType{userName, prayerScores}
	return createResultsFile(userName, &newUser)
}

// ListOfUserResultPaths ... Gets a list of all user files for scores in the directory
func ListOfUserResultPaths() (list []string) {
	RootFilePath = ConfigurationFile.UsersDirectory
	list, _ = listOfPaths(RootFilePath)
	return
}

// ListOfUserResults ... This produces a list of User Results
func ListOfUserResults() (listOfResults []UserResultsType) {
	listOfUserResultsPaths := ListOfUserResultPaths()
	for _, userResultPath := range listOfUserResultsPaths {
		listOfResults = append(listOfResults, GetUserResultsFromPath(userResultPath))
	}
	return
}

// GetUserResultsFromPath ... Returns the user results from the path
func GetUserResultsFromPath(resultsPath string) (resultsFile UserResultsType) {
	jsonFile, err := os.Open(resultsPath)
	if err == nil {
		byteArray, err := ioutil.ReadAll(jsonFile)
		if err == nil {
			json.Unmarshal(byteArray, &resultsFile)
		}
	}
	defer jsonFile.Close()
	return
}

// ListOfUsers ... Returns a list of users
func ListOfUsers() (list []string) {
	listOfUserResultPaths := ListOfUserResultPaths()
	var internalSlice StrSlice
	if len(listOfUserResultPaths) > 0 {
		for _, userResultPath := range listOfUserResultPaths {
			resultsFile := GetUserResultsFromPath(userResultPath)
			name := resultsFile.UserName
			if !internalSlice.Has(name) {
				internalSlice = append(internalSlice, name)
			}
		}
	}
	list = internalSlice
	return
}

// ListOfUsersWithAll...Returns a list of users with all at the top of the list
func ListOfUsersWithAll() (list []string) {
	listOfUsers := ListOfUsers()
	list = append([]string{"All"}, listOfUsers...)
	return
}

// GetResultsFromUser ... This returns the results from a user
func GetResultsFromUser(user string) (resultsFile UserResultsType) {

	listOfUserResults := ListOfUserResults()
	for _, userResults := range listOfUserResults {
		if userResults.UserName == user {
			resultsFile = userResults

			break
		}
	}
	return
}

// UserExists ... This returns true if the user exists false if they do not
func UserExists(user string) (userExists bool) {

	userExists = false
	listOfUsers := ListOfUsers()
	for _, foundUser := range listOfUsers {
		if foundUser == user {
			userExists = true
			return
		}
	}
	return
}

func getNumberOfMatches(scores []PrayerScoreType, group string, prayer string) (numberOfMatches int) {

	for _, result := range scores {
		prayerShouldBeIncluded := false

		if prayer != "All" {
			prayerShouldBeIncluded = prayer == "All" || result.PrayerName == prayer
		} else {
			prayerShouldBeIncluded = group == "All" || PrayerIsInGroup(result.PrayerName, group)
		}
		if prayerShouldBeIncluded {
			numberOfMatches++
		}
	}

	return
}

func adjustFilterInput(incomingGroup string, incomingPrayer string, incomingDateFilter string) (group string, prayer string, dateFilter string) {
	group = incomingGroup
	prayer = incomingPrayer
	dateFilter = incomingDateFilter
	if incomingGroup == "" {
		group = "All"
	}

	if incomingPrayer == "" {
		prayer = "All"
	}
	if incomingDateFilter == "" {
		dateFilter = "All"
	}
	return
}

func getFilteredResults(rawResults []PrayerScoreType, prayer string, group string, receivingString [][]string) (finalScores [][]string) {
	indexOfNewArray := 0
	for _, result := range rawResults {
		prayerShouldBeIncluded := false

		if prayer != "All" {
			prayerShouldBeIncluded = prayer == "All" || result.PrayerName == prayer
		} else {
			prayerShouldBeIncluded = group == "All" || PrayerIsInGroup(result.PrayerName, group)
		}

		if prayerShouldBeIncluded {
			receivingString[indexOfNewArray][0] = result.PrayerName
			receivingString[indexOfNewArray][1] = result.Score
			receivingString[indexOfNewArray][2] = result.DateAndTime
			indexOfNewArray++
		}
	}
	finalScores = receivingString

	return
}

func listOfUniquePrayerNames(scoreStrings [][]string) (results []string) {
	var accumulatedStrings StrSlice
	for _, result := range scoreStrings {
		if !accumulatedStrings.Has(result[0]) {
			accumulatedStrings = append(accumulatedStrings, result[0])
		}
	}
	results = accumulatedStrings
	return
}

func averagePercentage(scores []string) (finalScore string) {
	sumOfValues := 0.0
	numberOfEntries := len(scores)
	for _, score := range scores {
		strippedScore := strings.ReplaceAll(score, "%", "")
		extractedNumber, err := strconv.ParseFloat(strippedScore, 64)
		if err == nil {
			sumOfValues += extractedNumber
		}
	}
	average := sumOfValues / float64(numberOfEntries)

	finalScore = fmt.Sprintf("%.0f", average)
	finalScore = finalScore + "%"
	return
}

func filterForAverage(scoreStrings [][]string, uniquePrayerNames []string, sliceToHoldFinalScores [][]string) (results [][]string) {
	index := 0
	var tempScoreString []string
	for _, prayerName := range uniquePrayerNames {
		var scoreArray []string
		for _, scoreString := range scoreStrings {
			if scoreString[0] == prayerName {
				tempScoreString = scoreString
				scoreArray = append(scoreArray, scoreString[1])
			}
		}
		finalScore := averagePercentage(scoreArray)
		tempScoreString[1] = finalScore
		sliceToHoldFinalScores[index] = tempScoreString
		index++
	}
	results = sliceToHoldFinalScores
	return
}

func filterForLast(scoreStrings [][]string, uniquePrayerNames []string, sliceToHoldFinalScores [][]string) (results [][]string) {
	index := 0
	var tempScoreString []string
	for _, prayerName := range uniquePrayerNames {
		for _, scoreString := range scoreStrings {
			if scoreString[0] == prayerName {
				tempScoreString = scoreString
			}
		}
		sliceToHoldFinalScores[index] = tempScoreString
		index++
	}
	results = sliceToHoldFinalScores
	return
}

func filterForDateType(scoreStrings [][]string, dateFilter string) (results [][]string) {
	results = scoreStrings
	uniquePrayerNames := listOfUniquePrayerNames(scoreStrings)
	sizeOfCurrentMatches := len(uniquePrayerNames)
	if sizeOfCurrentMatches == 0 {
		sliceToHoldFinalScores := make([][]string, sizeOfCurrentMatches)
		results = sliceToHoldFinalScores
		return
	}
	sliceToHoldFinalScores := make([][]string, sizeOfCurrentMatches)
	for i := 0; i < sizeOfCurrentMatches; i++ {
		sliceToHoldFinalScores[i] = make([]string, 3)
	}

	if dateFilter == "Average" {
		sliceToHoldFinalScores = filterForAverage(scoreStrings, uniquePrayerNames, sliceToHoldFinalScores)
	}
	if dateFilter == "Last" {
		sliceToHoldFinalScores = filterForLast(scoreStrings, uniquePrayerNames, sliceToHoldFinalScores)
	}

	results = sliceToHoldFinalScores
	return

}

// GetListOfUniquePrayers ... This returns a list of unique prayer names for a particular user
func GetListOfUniquePrayers(user string) (finalPrayers []string) {
	finalScores := GetCurrentScores("All", user, "All", "All")
	var tempString StrSlice
	for _, score := range finalScores {
		if !tempString.Has(score[0]) {
			tempString = append(tempString, score[0])
		}
	}
	finalPrayers = tempString
	return
}

// GetListOfUniquePrayersThatAreInGroup ... This returns a list of unique prayer names for a particular user
func GetListOfUniquePrayersThatAreInGroup(user string, group string) (finalPrayers []string) {

	listOfUniquePrayers := GetListOfUniquePrayers(user)
	var localFinalPrayers StrSlice
	for _, prayer := range listOfUniquePrayers {
		prayerPath := GetPrayerPathFromName(prayer)
		var groupsFromPrayerPath StrSlice = GetGroupsFromPrayerPath(prayerPath)
		if groupsFromPrayerPath.Has(group) || group == "All" {
			if !localFinalPrayers.Has(prayer) {
				localFinalPrayers = append(localFinalPrayers, prayer)
			}
		}
	}

	finalPrayers = localFinalPrayers

	return
}

// GetListOfGroupsForUniquePrayers ... This returns a list of groups for the unique prayers
func GetListOfGroupsForUniquePrayers(user string) (finalGroups []string) {

	listOfPrayers := GetListOfUniquePrayers(user)
	var localGroups StrSlice

	for _, prayer := range listOfPrayers {
		prayerPath := GetPrayerPathFromName(prayer)
		groupsFromPrayerPath := GetGroupsFromPrayerPath(prayerPath)
		for _, group := range groupsFromPrayerPath {
			if !localGroups.Has(group) {
				localGroups = append(localGroups, group)
			}

		}

	}
	finalGroups = localGroups
	return

}

func GetCurrentScores(group string, user string, prayer string, dateFilter string) (finalScores [][]string) {

	currentResults := GetResultsFromUser(user).Scores

	group, prayer, dateFilter = adjustFilterInput(group, prayer, dateFilter)
	sizeOfCurrentMatches := getNumberOfMatches(currentResults, group, prayer)

	// I know this seems a little odd I have to figure out how big these need to be based on the
	// found items so once I determine the size I set aside the correct amount of memory in the
	// following loops.   That slice (that is empty but sized appropriately) is then passed into
	// getFilteredResults in order to obtain the filtered list
	if sizeOfCurrentMatches == 0 {
		sliceToHoldFinalScores := make([][]string, sizeOfCurrentMatches)
		finalScores = sliceToHoldFinalScores
		return
	}
	sliceToHoldFinalScores := make([][]string, sizeOfCurrentMatches)
	for i := 0; i < sizeOfCurrentMatches; i++ {
		sliceToHoldFinalScores[i] = make([]string, 3)
	}
	finalScores = getFilteredResults(currentResults, prayer, group, sliceToHoldFinalScores)
	if dateFilter != "All" {
		finalScores = filterForDateType(finalScores, dateFilter)
	}
	return
}

// GetMapOfUsersToPrayers ... This returns a map to associate users with all of their scored prayers
func GetMapOfUsersToPrayers() (mapOfUsers map[string][]string) {
	listOfUserResults := ListOfUserResults()
	mapOfUsers = make(map[string][]string)
	for _, userResults := range listOfUserResults {
		var accumulatedPrayers StrSlice
		for _, score := range userResults.Scores {
			if !accumulatedPrayers.Has(score.PrayerName) {
				accumulatedPrayers = append(accumulatedPrayers, score.PrayerName)
			}
		}

		mapOfUsers[userResults.UserName] = accumulatedPrayers
	}
	return
}

// GetPrayersFromUser ... This returns the prayer scores for a particular user will return false on exists if it does not exist
func GetPrayersFromUser(userName string) (listOfPrayers []string, exists bool) {
	if UserExists(userName) {
		mapOfUsersToPrayers := GetMapOfUsersToPrayers()
		listOfPrayers, exists = mapOfUsersToPrayers[userName]
	}
	return
}

// AddResultsToRecord ... This adds the results to the user record
func AddResultsToRecord(userName string, prayerScore PrayerScoreType) {

	if !UserExists(userName) {
		AddNewUser(userName)
	}
	resultsFromUser := GetResultsFromUser(userName)
	resultsFromUser.Scores = append(resultsFromUser.Scores, prayerScore)

	jsonFile, err := json.MarshalIndent(resultsFromUser, "", "\t")
	if err != nil {
		return
	}

	fileName := userName
	fileName = strings.ReplaceAll(fileName, " ", "_")

	if !directoryExists(ConfigurationFile.UsersDirectory) {
		os.MkdirAll(ConfigurationFile.UsersDirectory, os.ModePerm)
	}
	fullFilePath := ConfigurationFile.UsersDirectory + PS + fileName + ".json"
	fileHandler, insideErr := os.Create(fullFilePath)
	if insideErr != nil {
		err = insideErr
		return
	}
	bytes, err := fileHandler.Write(jsonFile)

	if bytes > 0 && err == nil {
		fileHandler.Close()
	}
}
