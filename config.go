package latinCatholicPrayers

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/user"
	"runtime"
	"strconv"
)

// Configuration ... This struct defines the configuration file
type Configuration struct {
	PrayerDirectory string
	DefaultGroup    string
	NumberOfOptions string
	SoundDirectory  string
	UsersDirectory  string
	HelpFiles       string
	DefaultPrayer   string
	DefaultUser     string
	AlternateUsers  []string
}

// ConfigurationFileName ... This is the string that dictates the name
// of the configuration name
var ConfigurationFileName string = "config.json"

// ConfigDir ... This is the config dir for the application
var ConfigDir string = ""

// ConfigurationFile .. This is the file that contains the configuration information for the
// application
var ConfigurationFile Configuration

func dirExists(path string) (found bool) {
	if path != "" {
		found = true
		_, err := os.Stat(path)
		if err != nil {
			found = false

		} else {
			if !isDir(path) {
				found = false
			}
		}
	}
	return
}

func isDir(directory string) (found bool) {
	fi, err := os.Stat(directory)

	if err != nil {

		return false
	}

	switch mode := fi.Mode(); {

	case !mode.IsDir():
		return false

	}

	return true

}

func isValidConfigFile(directory string) (found bool) {

	found = false
	configFilePath := directory + PS + ConfigurationFileName
	jsonFile, err := os.Open(configFilePath)
	if err == nil {
		byteArray, err := ioutil.ReadAll(jsonFile)
		if err == nil {
			err = json.Unmarshal(byteArray, &ConfigurationFile)
			if err == nil {
				found = true
			}
		}
	}

	if found {
		prelimNumberOfOptions, err := strconv.ParseInt(ConfigurationFile.NumberOfOptions, 10, 64)

		if err != nil {
			numberOfOptions = 5
		} else {
			numberOfOptions = int(prelimNumberOfOptions)
		}
	}

	return
}

func loadConfiguration(directory string) (found bool) {
	configDir := directory + PS
	if !isDir(configDir) {
		return false
	}

	return isValidConfigFile(directory)

}

func loadConfigurationFromWorkingDir() (found bool) {

	currentWorkingDirectory, err := os.Getwd()
	if err != nil {
		return false
	}
	ConfigDir = currentWorkingDirectory
	return loadConfiguration(currentWorkingDirectory)
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)

	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func saveConfigFile() {
	userConfig := ConfigDir + PS + "config.json"
	jsonFile, err := json.MarshalIndent(ConfigurationFile, "", "\t")
	fileHandler, err := os.Create(userConfig)
	if err == nil {
		fileHandler.Write(jsonFile)
		defer fileHandler.Close()
	}
}

func createNewUserConfig(configDir string) (created bool) {
	userConfig := configDir + PS + "config.json"
	if !dirExists(configDir) {
		os.MkdirAll(configDir, os.ModePerm)
	}

	err := setupDefaultConfigEntries(userConfig)
	if err != nil {
		return
	}
	return
}

func setupDefaultConfigEntries(configDir string) (err error) {
	userConfig := configDir + PS + "config.json"
	baseDir := PS + "opt" + PS + "chrisfair" + PS + "latin-catholic-prayers"
	homeDir, _ := os.UserHomeDir()
	homeConfigDir := homeDir + PS + ".config" + PS + "latin-catholic-prayers"
	if runtime.GOOS == "windows" {
		baseDir = "C:" + PS + "Program Files (x86)" + PS + "chrisfair" + PS + "latin-catholic-prayers"
	}

	if err == nil {
		user, err := user.Current()

		if ConfigurationFile.UsersDirectory == "" {
			ConfigurationFile.UsersDirectory = homeConfigDir + PS + "UserResults"
		}
		if ConfigurationFile.DefaultPrayer == "" {
			ConfigurationFile.DefaultPrayer = "Signum Crucis"
		}
		if ConfigurationFile.DefaultGroup == "" {
			ConfigurationFile.DefaultGroup = "All"
		}
		if ConfigurationFile.HelpFiles == "" {
			ConfigurationFile.HelpFiles = baseDir + PS + "HelpFiles"
		}

		if ConfigurationFile.NumberOfOptions == "" {
			ConfigurationFile.NumberOfOptions = "5"
		}

		if ConfigurationFile.SoundDirectory == "" {
			ConfigurationFile.SoundDirectory = baseDir + PS + "Sounds"
		}

		if ConfigurationFile.PrayerDirectory == "" {
			ConfigurationFile.PrayerDirectory = baseDir + PS + "Prayers"
		}

		if ConfigurationFile.DefaultUser == "" {
			ConfigurationFile.DefaultUser = user.Name
		}
		jsonFile, err := json.MarshalIndent(ConfigurationFile, "", "\t")
		fileHandler, err := os.Create(userConfig)
		if err == nil {
			fileHandler.Write(jsonFile)
			defer fileHandler.Close()
		}
	}
	return
}

func loadConfigurationFromUser() (found bool) {
	user, err := user.Current()
	if err != nil {
		return false
	}

	configDir := user.HomeDir +
		PS + ".config" + PS + "latin-catholic-prayers"
	found = loadConfiguration(configDir)

	if !found {
		found = createNewUserConfig(configDir)
	}

	found = (setupDefaultConfigEntries(configDir) == nil) && found // any error returned will not be false
	ConfigDir = configDir
	return
}

// LoadConfiguration ... This is the function that determines which configuration to
// load and then loads it
func LoadConfiguration() (found bool) {

	found = false

	found = loadConfigurationFromWorkingDir()
	if found {
		return
	}

	found = loadConfigurationFromUser()
	if found {
		return
	}

	return

}
