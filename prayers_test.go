package latinCatholicPrayers

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ListOfPrayerPaths(t *testing.T) {

	ConfigurationFile.PrayerDirectory = "PrayersTest"
	tests := []struct {
		name     string
		wantList []string
		wantErr  bool
	}{

		{"CurrentList",
			[]string{"PrayersTest/Basic/PaterNoster/PaterNoster.json",
				"PrayersTest/Basic/TestPrayer/TestPrayer.json",
				"PrayersTest/Basic/TestPrayer2/TestPrayer2.json",
			},
			false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotList, err := ListOfPrayerPaths()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListOfPrayerPaths() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotList, tt.wantList) {
				t.Errorf("ListOfPrayerPaths() = %v, want %v", gotList, tt.wantList)
			}
		})
	}
}

func Test_SetLanguageType(t *testing.T) {
	SetLanguageType("Latin")
	assert.Equal(t, Latin, CurrentLanguageSelection, "Should be Latin")
	SetLanguageType("English")
	assert.Equal(t, English, CurrentLanguageSelection, "Should be English")
	assert.NotEqual(t, Latin, CurrentLanguageSelection, "Should be not be English")

}

func Test_Has(t *testing.T) {

	testSlice := StrSlice{"Apples", "Grapes", "Pears"}
	assert.False(t, testSlice.Has("Monkeys"), "List should not have monkeys")
	assert.True(t, testSlice.Has("Apples"), "Should have apples")

}

func Test_GetListOfGroups(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	listOfGroups := GetListOfGroups()
	assert.Equal(t, 8, len(listOfGroups), "Should currently be 8 groups")
	assert.Contains(t, listOfGroups, "All", "Allways contains all")
	assert.Contains(t, listOfGroups, "Basic", "Basic should exist as it is basic prayers")
	assert.Contains(t, listOfGroups, "Marian", "Marian currently exists")
	assert.NotContains(t, listOfGroups, "DevilStuff", "Better not have any devil stuff!")
}

func Test_PrayerAsArray(t *testing.T) {
	SetLanguageType("Latin")
	ConfigurationFile.PrayerDirectory = "Prayers"
	prayerAsArray := RetrievePrayerArray("Pater Noster")
	assert.Contains(t, prayerAsArray, "Pater", "First word should be Pater")

}

func Test_SetSelectedPrayer(t *testing.T) {
	SetLanguageType("Latin")
	ConfigurationFile.PrayerDirectory = "Prayers"
	localSelectedPrayer := "Gloria Patri"
	SetSelectedPrayer(localSelectedPrayer)
	assert.Equal(t, localSelectedPrayer, SelectedPrayer, "Should be gloria patri")
	assert.NotEqual(t, "Devils Chant", SelectedPrayer, "Should have nothing to do with devil")

}
func Test_GetPrayerNamesFromGroup(t *testing.T) {
	SetLanguageType("Latin")
	ConfigurationFile.PrayerDirectory = "Prayers"
	listOfPrayersForAll := GetPrayerNamesFromGroup("All")
	listOfPrayersForRosary := GetPrayerNamesFromGroup("Rosary")
	listOfPrayersForBasic := GetPrayerNamesFromGroup("Basic")
	assert.Contains(t, listOfPrayersForAll, "Gloria Patri")
	assert.Contains(t, listOfPrayersForBasic, "Gloria Patri")
	assert.NotContains(t, listOfPrayersForRosary, "Gloria Patri")
}

func Test_finalizeQuiz(t *testing.T) {

	finalizeQuiz()
	assert.Equal(t, 0, currentQuizItem, "Should be 0")
	assert.Equal(t, 0, numberOfWrongAnswers, "Should be 0")
	assert.Equal(t, "", completedQuiz, "Should be empty string")
	assert.Equal(t, 0, currentScore, "Should be zero")

}
func Test_InitializeQuiz(t *testing.T) {
	SetLanguageType("Latin")
	ConfigurationFile.PrayerDirectory = "Prayers"
	testGoodPrayer := "Pater Noster"
	testBadPrayer := "Devils Chant"
	goodResult := InitializeQuiz(testGoodPrayer)
	badResult := InitializeQuiz(testBadPrayer)
	assert.True(t, goodResult, "Should work without a problem")
	assert.False(t, badResult, "Should not work")

	SetLanguageType("English")
	goodResult = InitializeQuiz(testGoodPrayer)
	badResult = InitializeQuiz(testBadPrayer)
	assert.True(t, goodResult, "Should work without a problem")
	assert.False(t, badResult, "Should not work")

}

func Test_GetNameFromPrayerPath(t *testing.T) {
	type args struct {
		prayerPath string
	}
	SetLanguageType("English")
	goodPrayerPath := args{"PrayersTest" + PS + "Basic" + PS + "TestPrayer" + PS + "TestPrayer.json"}

	tests := []struct {
		name     string
		args     args
		wantName string
	}{
		{"Retrieve English Name ",
			goodPrayerPath,
			"EnglishName",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotName := GetNameFromPrayerPath(tt.args.prayerPath); gotName != tt.wantName {
				t.Errorf("GetNameFromPrayerPath() = %v, want %v", gotName, tt.wantName)
			}
		})
	}
}

func Test_RetrieveLatinPrayer(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		name string
	}
	prayer := args{"EnglishName"}
	tests := []struct {
		name            string
		args            args
		wantLatinPrayer string
	}{
		{"Retrieve Latin prayer", prayer, "Et filii"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotLatinPrayer := RetrieveLatinPrayer(tt.args.name); gotLatinPrayer != tt.wantLatinPrayer {
				t.Errorf("RetrieveLatinPrayer() = %v, want %v", gotLatinPrayer, tt.wantLatinPrayer)
			}
		})
	}
}

func Test_GetEnglishNameFromPrayerPath(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		prayerPath string
	}

	prayer := args{ConfigurationFile.PrayerDirectory + PS + "Basic" + PS + "TestPrayer" + PS + "TestPrayer.json"}
	tests := []struct {
		name     string
		args     args
		wantName string
	}{

		{"Retrieve English Name", prayer, "EnglishName"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotName := GetEnglishNameFromPrayerPath(tt.args.prayerPath); gotName != tt.wantName {
				t.Errorf("GetEnglishNameFromPrayerPath() = %v, want %v", gotName, tt.wantName)
			}
		})
	}
}

func Test_PrayerIsInGroup(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	SetLanguageType("Latin")
	assert.True(t, PrayerIsInGroup("Pater Noster", "Basic"))
	assert.True(t, PrayerIsInGroup("Pater Noster", "Mass"))
	assert.False(t, PrayerIsInGroup("Pater Noster", "Marian"))
}

func Test_ListOfPrayerNames(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	SetLanguageType("Latin")
	listOfPrayerNames, err := ListOfPrayerNames()
	assert.Nil(t, err)
	assert.Contains(t, listOfPrayerNames, "Pater Noster")
	assert.NotContains(t, listOfPrayerNames, "Devils Prayer")

}

func Test_GroupOfPrayerPaths(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	SetLanguageType("Latin")
	listOfPrayerPaths, err := GroupOfPrayerPaths("Basic")
	assert.Nil(t, err)
	assert.Contains(t, listOfPrayerPaths, "Prayers/PaterNoster.json")
	assert.NotContains(t, listOfPrayerPaths, "Prayers/DevilsPrayer.json")

}

func Test_GroupOfPrayerNames(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	SetLanguageType("Latin")
	listOfPrayerNames, err := GroupOfPrayerNames("Basic")
	assert.Nil(t, err)
	assert.Contains(t, listOfPrayerNames, "Pater Noster")
	assert.NotContains(t, listOfPrayerNames, "Devils Prayer")

}

func Test_GetLatinNameFromPrayerPath(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "PrayersTest"
	type args struct {
		prayerPath string
	}
	prayer := args{ConfigurationFile.PrayerDirectory + PS + "Basic" + PS + "TestPrayer" + PS + "TestPrayer.json"}
	tests := []struct {
		name     string
		args     args
		wantName string
	}{
		{"Retrieve Latin Name", prayer, "LatinName"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotName := GetLatinNameFromPrayerPath(tt.args.prayerPath); gotName != tt.wantName {
				t.Errorf("GetLatinNameFromPrayerPath() = %v, want %v", gotName, tt.wantName)
			}
		})
	}
}
