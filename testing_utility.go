package latinCatholicPrayers

func thereAreDuplicates(list []string) (duplicates bool) {

	duplicateFrequency := make(map[string]int)

	for _, item := range list {
		// check if the item/element exist in the duplicateFrequency map

		_, duplicates = duplicateFrequency[item]

	}
	return
}
