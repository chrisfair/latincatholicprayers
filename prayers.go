package latinCatholicPrayers

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// RootFilePath ... This is the path that leads to the root of the program
var RootFilePath string

// PS ... This is a constant referring to the os path separator
// PS ... This is a constant referring to the os path separator
const PS string = string(os.PathSeparator)

// Prayer ... parsed from the json file
type Prayer struct {
	Latin       string
	English     string
	AudioFile   string
	EnglishName string
	LatinName   string
	Groups      []string
}

// LanguageType ... This is a custom type indicating whether this is an English or Latin prayer
type LanguageType int

// Engish .. This to select the English prayer type
// Latin .. This is to select the Latin prayer type
const (
	English LanguageType = iota
	Latin
)

// CurrentLanguageSelection ... This variable contains the prayer type currently being viewed
var CurrentLanguageSelection LanguageType = Latin

// SetLanguageType ... Permits the prayer type to be set to either English or Latin
func SetLanguageType(prayerType string) (language LanguageType) {
	if prayerType == "English" {
		CurrentLanguageSelection = English
	} else {
		CurrentLanguageSelection = Latin
	}
	language = CurrentLanguageSelection
	return
}

// GetListOfGroups ... This returns a list of all groups found in the directories
func GetListOfGroups() (reportedGroups []string) {

	listOfPrayerPaths, err := ListOfPrayerPaths()
	var internalSlice StrSlice
	if err == nil {
		for _, prayerPath := range listOfPrayerPaths {
			names := GetGroupsFromPrayerPath(prayerPath)
			for _, name := range names {
				if !internalSlice.Has(name) {
					internalSlice = append(internalSlice, name)
				}
			}
		}
	}
	internalSlice = append(internalSlice, "All")
	reportedGroups = internalSlice
	return
}

// GetPrayerNamesFromGroup ... This produces a liet of prayer names associated with a group
func GetPrayerNamesFromGroup(group string) (reportedNames []string) {

	var localNames StrSlice
	listOfPrayerPaths, err := ListOfPrayerPaths()
	if err == nil {
		if err == nil {
			for _, prayerPath := range listOfPrayerPaths {
				foundGroups := GetGroupsFromPrayerPath(prayerPath)
				for _, foundGroup := range foundGroups {
					prayerName := GetNameFromPrayerPath(prayerPath)
					if (foundGroup == group) || (group == "All") {
						if !localNames.Has(prayerName) {
							localNames = append(localNames, prayerName)
						}
					}
				}
			}
		}
	}
	reportedNames = localNames
	return
}

func listOfPaths(path string) (list []string, err error) {
	RootFilePath = path
	err = filepath.Walk(RootFilePath, func(path string, info os.FileInfo, err error) error {
		if strings.ToLower(filepath.Ext(path)) == ".json" {
			list = append(list, path)
		}
		return nil
	})

	return

}

// ListOfPrayerPaths ... This returns a list of prayer paths
func ListOfPrayerPaths() (list []string, err error) {
	RootFilePath = ConfigurationFile.PrayerDirectory
	list, err = listOfPaths(RootFilePath)
	return
}

// GetPrayerFromPrayerPath ... This returns the prayer as the Prayer struct if it finds
// it in the directory
func GetPrayerFromPrayerPath(prayerPath string) (prayer Prayer, err error) {
	jsonFile, err := os.Open(prayerPath)
	if err == nil {
		byteArray, err := ioutil.ReadAll(jsonFile)
		if err == nil {
			json.Unmarshal(byteArray, &prayer)
		}
	}
	defer jsonFile.Close()
	return
}

func GetListOfPrayers(group string) (prayers []string) {
	prayers = GetPrayerNamesFromGroup(group)
	return
}

// PrayerIsInGroup ... returns true if the prayer is in the group and false if it is not
func PrayerIsInGroup(prayer string, group string) (isInGroup bool) {
	var listOfPrayers StrSlice = GetListOfPrayers(group)
	isInGroup = listOfPrayers.Has(prayer)
	return
}

// GetNameFromPrayerPath ... Given a particular path to a prayer return the
// name of the prayer from the file.   If one is not found return empty string
func GetNameFromPrayerPath(prayerPath string) (name string) {
	name = ""
	prayer, err := GetPrayerFromPrayerPath(prayerPath)
	if err == nil {
		if CurrentLanguageSelection == English {
			name = prayer.EnglishName
		} else {
			name = prayer.LatinName
		}
	}
	return
}

// GetEnglishNameFromPrayerPath ... Given a particular path to a prayer return the
// name of the prayer from the file.   If one is not found return empty string
func GetEnglishNameFromPrayerPath(prayerPath string) (name string) {

	name = ""
	prayer, err := GetPrayerFromPrayerPath(prayerPath)
	if err == nil {
		name = prayer.EnglishName
	}
	return
}

// GetLatinNameFromPrayerPath ... Given a particular path to a prayer return the
// name of the prayer from the file.   If one is not found return empty string
func GetLatinNameFromPrayerPath(prayerPath string) (name string) {
	name = ""
	prayer, err := GetPrayerFromPrayerPath(prayerPath)
	if err == nil {
		name = prayer.LatinName
	}
	return
}

// GetPrayerPathFromName ... This returns a prayer path when provided the prayer name
// this will always return the first found prayer path so if there are multiple prayers with the same name it will
// not work properly
func GetPrayerPathFromName(prayerName string) (prayerPath string) {

	listOfPrayerPaths, err := ListOfPrayerPaths()
	if err == nil {
		for _, path := range listOfPrayerPaths {
			prayerNameFound := GetNameFromPrayerPath(path)
			if prayerNameFound == prayerName {
				prayerPath = path
				break

			}
		}

	}
	return
}

// GetLatinNameFromPrayerPathAndExpectedName ... Returns the prayer path given the expected name
// and the prayer path for the name
func GetLatinNameFromPrayerPathAndExpectedName(prayerPath string, expectedName string) (latinName string) {
	latinName = GetLatinNameFromPrayerPath(prayerPath)
	englishName := GetEnglishNameFromPrayerPath(prayerPath)
	if (latinName == expectedName) || (englishName == expectedName) {
		latinName = expectedName
		return
	}
	return
}

// GetGroupsFromPrayerPath ... This obtains the group from a prayer in a particular path
func GetGroupsFromPrayerPath(prayerPath string) (groups []string) {
	var prayer Prayer
	jsonFile, err := os.Open(prayerPath)
	if err == nil {
		byteArray, err := ioutil.ReadAll(jsonFile)
		json.Unmarshal(byteArray, &prayer)
		if err == nil {
			groups = prayer.Groups
		}
	}

	defer jsonFile.Close()

	return

}

// ListOfPrayerNames ... Returns a list of prayer names that exist
func ListOfPrayerNames() (list []string, err error) {
	listOfPrayerPaths, err := ListOfPrayerPaths()
	if err == nil {
		for _, prayerPath := range listOfPrayerPaths {
			name := GetNameFromPrayerPath(prayerPath)
			list = append(list, name)
		}
	}
	return
}

// GroupOfPrayerPaths ... Retrieves a list of prayer paths that belong to a particular group as indicated
// in the 'Group' designation of the json file
func GroupOfPrayerPaths(group string) (list []string, err error) {
	listOfPrayerPaths, err := ListOfPrayerPaths()
	if err == nil {
		for _, prayerPath := range listOfPrayerPaths {
			foundGroups := GetGroupsFromPrayerPath(prayerPath)
			for _, foundGroup := range foundGroups {
				if strings.ToLower(foundGroup) == strings.ToLower(group) {
					list = append(list, prayerPath)
				}
			}
		}
	}
	return
}

// GroupOfPrayerNames ... Retrieves a list of prayer paths that belong to a particulary group as indicated
// in the 'Group' designation of the json file
func GroupOfPrayerNames(group string) (list []string, err error) {
	listOfPrayerPaths, err := GroupOfPrayerPaths(group)
	if err == nil {
		for _, prayerPath := range listOfPrayerPaths {
			name := GetNameFromPrayerPath(prayerPath)
			list = append(list, name)
		}
	}
	return
}

func retrievePrayer(name string, prayerType LanguageType) (prayerString string) {
	var prayer Prayer
	listOfPrayerPaths, err := ListOfPrayerPaths()
	if err == nil {
		for _, prayerPath := range listOfPrayerPaths {
			nameFound := GetEnglishNameFromPrayerPath(prayerPath)
			if nameFound != name {
				nameFound = GetLatinNameFromPrayerPath(prayerPath)
			}
			if strings.ToLower(nameFound) == strings.ToLower(name) {
				prayer, err = GetPrayerFromPrayerPath(prayerPath)
				if err == nil {

					switch prayerType {
					case English:
						prayerString = prayer.English
					case Latin:
						prayerString = prayer.Latin
					default:
						prayerString = ""
					}
					break
				}
			}
		}
	}
	return

}

// RetrievePrayerArray ... Public facing function to return a string for the currently selected
// prayer as the correct prayer type as an array
func RetrievePrayerArray(name string) (prayerAsArray []string) {
	prayerAsString := retrievePrayer(name, CurrentLanguageSelection)
	prayerAsArray = strings.Fields(prayerAsString)
	return
}

// RetrieveEnglishPrayer ... Retrieves a prayer based on the name of to the prayer
func RetrieveEnglishPrayer(name string) (englishPrayer string) {
	return retrievePrayer(name, English)
}

// RetrieveLatinPrayer ... Retrieves a prayer based on the English Name or the Latin Name
func RetrieveLatinPrayer(name string) (latinPrayer string) {
	return retrievePrayer(name, Latin)
}
